import pygame
import random
import time
from math import sin, cos, pi

pygame.init()

WHITE = (255, 255, 255)
BLACK = (000, 000, 000)
RED = (255, 000, 000)
GREEN = (000, 255, 000)
BLUE = (000, 000, 255)
size = (600, 400)
score = 0
max_score = 0
game_time = 0
start = time.time()
no_print_statement = 0
font = pygame.font.Font(None, 30)

screen = pygame.display.set_mode(size)
pygame.display.set_caption("Test Game")
pygame.mouse.set_visible(False)


class Block(pygame.sprite.Sprite):
    def __init__(self, color, width, height):
        super().__init__()
        self.image = pygame.Surface([width, height])
        self.image.fill(color)
        self.rect = self.image.get_rect()
        # self.pi = pi
        self.A = random.uniform(-2*pi, 2*(pi))
        self.offsetx = random.randrange(0, 400)
        self.offsety = random.randrange(0, 600)
        self.morerandom = random.randrange(0, 2)
        self.speedC = 0




        # self.xoffset = 1
        # self.yoffset = 1
        # self.randomx = random.randrange(0, 10)
        # self.randomy = random.randrange(0, 10)
        # self.linex = cos(self.A)
        # self.liney = sin(self.A)
        self.speed = random.randrange(1, 5)

    def reset_pos(self):
        self.A = random.uniform(-2 * pi, 2 * (pi))
        # self.offsetx = random.randrange(0, 400)
        # self.offsety = random.randrange(0, 600)
        self.random = random.randrange(0, 2)
        self.speed = random.randrange(1, 5)
        self.rect.y = random.randrange(-30, 430)
        if self.rect.y < 0 or self.rect.y > 400:
            self.rect.x = random.randrange(-30, 630)
        elif self.rect.y > 0 or self.rect.y < 400:
            if self.random == 1:
                self.rect.x = -1*random.randrange(0, 30)
            else:
                self.rect.x = random.randrange(600, 630)





         # if self.random == 0:
         #     self.rect.y = random.randrange(-30, -10)
         #     print("insidehere")
         # else:
         #     self.rect.y = random.randrange(410, 430)
         #     print("insidehere2")
         #
         # if self.random1 == 0:
         #     self.rect.x = random.randrange(0, 0)
         #     print("insidehere3")
         # else:
         #     self.rect.x = random.randrange(610, 630)
         #      print("insidehere3")

    def update(self):
        if self.morerandom == 0:
            self.speedC += self.speed
        else:
            print("inside")
            self.speedC -= self.speed
        self.rect.x = self.speedC*cos(self.A) #+ self.offsetx
        self.rect.y = self.speedC*sin(self.A) #+ self.offsety
        # self.rect.x += self.linex*self.speed
        # self.rect.y += self.liney*self.speed
        if self.rect.y >= 460 or self.rect.y <= -60 or self.rect.x >= 660 or self.rect.x <= -60:
            self.reset_pos()
        # self.rect.x += self.randomx
        # self.rect.y += self.randomy


         # self.rect.x = self.xoffset*cosh(self.A)
         # self.rect.y = self.yoffset*sinh(self.A)
         # self.xoffset += 1
         # self.yoffset += 1



        # self.rect.y += speed
        # if self.rect.y > 400:
        #     self.reset_pos()


def game_timer(start):
    game_time = time.time() - start
    return game_time


block_lst = pygame.sprite.Group()
all_sprite_lst = pygame.sprite.Group()
reset_block_lst = pygame.sprite.Group()

for i in range(25):
    black_block = Block(BLACK, 20, 40)
    randomnum = random.randrange(0, 2)
    randomnum = random.randrange(0, 2)
    black_block.rect.y = random.randrange(-10, 410)
    if black_block.rect.y < 0 or black_block.rect.y > 400:
        black_block.rect.x = random.randrange(-10, 610)
    elif black_block.rect.y > 0 or black_block.rect.y < 400:
        if randomnum == 1:
            black_block.rect.x = -1 * random.randrange(0, 10)
        else:
            black_block.rect.x = random.randrange(600, 610)
    black_block.add(block_lst)
    black_block.add(all_sprite_lst)
    max_score += 1

reset_block = Block(WHITE, 20, 20)
reset_block.add(reset_block_lst)
reset_block.add(all_sprite_lst)
reset_block.rect.y = 0
reset_block.rect.x = 0

player = Block(RED, 10, 15)

player.add(all_sprite_lst)


done = False

clock = pygame.time.Clock()


while not done:

    screen.fill(WHITE)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    pos = pygame.mouse.get_pos()
    player.rect.x = pos[0]
    player.rect.y = pos[1]

    reseter_lst = pygame.sprite.spritecollide(reset_block, block_lst, False)
    for black_block in reseter_lst:
        black_block.reset_pos()


    block_hit_lst = pygame.sprite.spritecollide(player, block_lst, True)

    for black_block in block_hit_lst:
        score += 1
        print(score)
        black_block.reset_pos()

    all_sprite_lst.draw(screen)

    block_lst.update()

    if score == max_score:
        if no_print_statement == 0:
            stop_time = game_time
            no_print_statement += 1
        timer_score = "You gathered all the blocks in a time of:"
        textImg = font.render(timer_score, 32, BLACK)
        textImg1 = font.render((str(int(stop_time)) + "seconds"), 32, GREEN)
        screen.blit(textImg, [100, 200])
        screen.blit(textImg1, [175, 220])

    scoreImg = font.render(str(score), 32, BLUE)
    timer = font.render(str(int(game_time)), 32, BLUE)
    screen.blit(scoreImg, [0,0])
    screen.blit(timer, [580, 0])

    pygame.display.flip()

    clock.tick(60)
    game_time = game_timer(start)


pygame.quit()